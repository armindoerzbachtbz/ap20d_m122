![TBZ Logo](./x_gitressourcen/tbz_logo.png)
![m319 Picto](./x_gitressourcen/m122_picto.png)

[TOC]

# M122 - Abläufe mit einer Scriptsprache automatisieren

## Modulidentifikation
[Link](https://www.modulbaukasten.ch/?q=122)

## Einfuehrung
[Link](Einfuehrung.md)

## Lektionenplan
| Tag  |  Thema | Bemerkung: Auftrag, Übungen |
|:----:|:------ |:-------------- |
|  25.05.23   |  [Linux Einführung](./01_Linux_Einf/README.md) <br> [Bash Grundlagen: Teil 1](./02_Bash_Grundl/README.md#teil-1-linuxbefehle) | Virtualisierung (oder Raspberry Pi) & Linux Distribution installieren <br> Erste Schritte mit Bash <br>  Bash Grundlagen |
|  01.06.23   | [Bash Grundlagen: Teil 1](./02_Bash_Grundl/README.md#teil-1-linuxbefehle) | Bash Grundlagen <br> Checkpoints mit Übungen --> Lösungen |
|  08.06.23   |  **LB0 (20%)** <br> [Bash Grundlagen](./02_Bash_Grundl/README.md) <br> [Bash Übungen](./03_Bash_Ueb/README.md) | **LB0: Bash Grundlagen: Teil 1 auf Ecolm** <br> Bash Grundlagen <br> Checkpoints mit Übungen --> Lösungen |
|  15.06.23   | [Bash Aufgaben](./04_Bash_Aufg/README.md) <br> [Vorbereitung LB1 & Lösungen](./05_Bash_Vorb_LB1/README.md) | Aufgaben 1 & 2 <br> Repetition, Aufgaben fertig --> Lösungen |
|  22.06.23   | **LB1 (30%)** <br> **LB2 (50%) Start: [Projekt](./10_Projekte_LB2/README.md)** <br> P-Antrag <br> P-Design  | **LB1: Bash Grundlagen: Teil 2 und 3 auf Ecolm** <br> Start von LB2<br> **Meilenstein A** Abgabe Projektantrag |
|  29.06.23   | P-Design <br> P-Code | **Meilenstein B** Abgabe Design (Aktivitätsdiagramm aka m319) |
|  06.07.23   | P-Code |  |
|  13.07.23   | **Projektabgabe** <br> Demo, P-Doku, P-Test | **Meilenstein C** Projektdemo & Abgabe Code mit Kommentar (TBZ Code Conventions) <br> **Meilenstein D** Abgabe: Testbericht und Doku                      |


## Projekt Rahmenbedingungen

* Kann in einer beliebigen Scriptsprache umgesetzt werden, vorzugsweise in Bash! (Alternativen falls bekannt: Python für APIs)
* Themen: Benutzerverwaltung, Netzwerk, ...
* Die Scripts sollten automatisch gestartet werden (CronJob).
* Einstellungen via Config-Dateien.
* Eingaben (inkl. CL Args) müssen robust sein, d.h. Fehler sind mit Fehlermeldungen quittiert.

## Übung 5 - Für Fortgeschrittene:

 - Was macht folgender Ausdruck?
		`dmesg | egrep '[0-9]{4}:[0-9]{2}:[0-9a-f]{2}.[0-9]'`

 - Was macht folgender Ausdruck?
		`ifconfig | grep -oE '((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])'`




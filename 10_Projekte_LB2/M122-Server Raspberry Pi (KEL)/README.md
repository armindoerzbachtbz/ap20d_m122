![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../../x_gitressourcen/m122_picto.png)

[TOC]

# M122-Server - Dokumentation

---

# Umsetzung folgender Ideen möglich (BASH / Linux):

Folgend Ideen zur **Erhebung von RAW-Daten** vom Kundensystems / -dienstes (Linux Server, Raspberry Pi, Web-API):

- SQL-Daten: Serverstatus, Berechtigungen, Tabellendaten bestimmter Datenbanken (.sql), ...
- Strukturierte Daten: Dateisystem, Berechtigungen, .cvs, .json, ...
- Status / Performance Daten: Systemdaten, Daten von Perfomance-Tools, Dienstdaten, ...
- IoT-Daten: **Sensordaten**

Folgend Ideen zur **Verarbeitung und Übermittlung von FMT-Daten** an Kundensystems / -dienstes (Linux Server, Raspberry Pi, Web-Dienst):

- Publish: Darstellen der Daten *WEB*
- DB Storage: Datenbank SQL insert, ...


## M122-Server-Dienste:

Im Schulzimmer läuft ein Server mit folgenden Zugängen. Ihr könnt auf alle Statusinformationen (Linux) zugreifen, und Daten auf folgenden Diensten beliebig vorgeben, laden, verändern:

- **SSH**: Zugriff auf Server (auch sftp)
	
- **MySQL** Server mit Zugang via **Sql-Client** oder **phoMyAdmin** oder **Workbench**
	
- **IoT**: Sensoren (Temp, Feuchtigkeit, Licht, Sound, ...) werden permanent ausgelesen und in Datafiles zur Verfügung gestellt.
	
- **Web-Server**: Zeigt Webseiten aus Ordnern. an. (Z.B. für Publishing)
	
- **FTP**: Zugriff auf Ordnerstruktur: Eigene Data-Ordner, Website-Ordner, IoT-Daten

 
Hier noch ein Link zu freien WEB-APIs für Download von akt. Daten:
<https://rapidapi.com/collection/list-of-free-apis>

## Zugang Online: 
Unter der Woche (gemäss Absprache) ist der Server Online. unter **kel.internet-box.ch**. Siehe [Zugänge](https://gitlab.com/ch-tbz-it/Stud/m122/-/blob/main/10_Projekte_LB2/M122-Server%20Raspberry%20Pi%20(KEL)/Zug%C3%A4nge%20&%20Dienst%20M122-Server.xlsx) für Details.

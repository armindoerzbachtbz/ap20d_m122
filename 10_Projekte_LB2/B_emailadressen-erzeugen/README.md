# M122 - Aufgabe

2023-02 MUH


## Emailadressen und Brief erstellen

Files, Listen/Arrays, String-Operationen, Archive, Mailing, FTP, Print
<br>
<br>


**Warnung**:<br>
<mark>Unterschätzen Sie die Aufgabe nicht! Es kann gut sein, dass Sie weit 
über *8 Stunden* Entwicklungs- und Testzeit aufwenden müssen (der Teufel 
liegt im Detail). Grade das Mailen und der FTP-Transfer kann tückisch sein!
Es ist die grosse Menge an Kleinigkeiten, die Sie lösen und beherrschen müssen.
</mark>

<br>
<br>

### Ausgangslage

Sie sind in der internen Informatik der TBZ tätig und 
Sie bekommen für den bevorstehenden Schulanfang eine
[**Liste von Namen (MOCK_DATA.csv)**](MOCK_DATA.csv), 
die alle eine neue Emailadressen bekommen sollen.

Zur **Emailadresse** soll auch ein **Initialpasswort** 
generiert werden. Emailadressen und Passwörter
müssen "korrekte" Zeichen enthalten. Also keine
Klammern, Apostrophe, Akzente, Leerzeichen usw. 
Aber Vorsicht: Die Inputdaten sind nicht rein, das
ist auch eine Ihrer Aufgaben, dieses Problem zu lösen.


### Stufe 1

Sie müssen, um das System zu füttern, eine 
Liste aller Emailadressen und das (dazugehörige,
automatisch generierte) Passwörter in einer
Datei namens <br>**YYYY-MM-DD_HH-SS_mailimports.csv**
erstellen lassen.

	[GenerierteEmailadresse1];[GeneriertesPasswort1]
	[GenerierteEmailadresse2];[GeneriertesPasswort2]
	...
	[GenerierteEmailadresse999];[GeneriertesPasswort999]


### Stufe 2

Alle Personen, die jetzt eine neue Emailadresse und Passwort bekommen,
sollen per Papierbrief benachrichtigt werden. Erstellen Sie pro neue
Emailadresse folgende Datei <br>**YYYY-MM-DD_HH-SS_[GenerierteEmailadresse].brf** 
<br><br>(Die Anschriftadresse passt in ein Fenster-Kuvert, 
die Distanzen für das Einrücken, machen Sie mit Leerzeichen)

	Technische Berufsschule Zürich
	Ausstellungsstrasse 70
	8005 Zürich
	
	Zürich, den [DD.MM.YYYY]
	
							[Vorname] [Nachname]
							[Strasse] [StrNummer]
							[Postleitzahl] [Ort]
									
	
	Liebe:r [Vorname]
	
	Es freut uns, Sie im neuen Schuljahr begrüssen zu dürfen.
	
	Damit Sie am ersten Tag sich in unsere Systeme einloggen
	können, erhalten Sie hier Ihre neue Emailadresse und Ihr
	Initialpasswort, das Sie beim ersten Login wechseln müssen.
	
	Emailadresse:   [GenerierteEmailadresse]
	Passwort:       [GeneriertesPasswort]
	
	
	Mit freundlichen Grüssen
	
	[IhrVorname] [IhrNachname]
	(TBZ-IT-Service)
	
	
	admin.it@tbz.ch, Abt. IT: +41 44 446 96 60



### Stufe 3

Erstellen Sie eine "Archiv"-Datei von **allen Dateien**, also
von der Mail- und Passwortliste und auch von allen Briefen. 
Der "Archiv"-Dateiname soll so aussehen:
<br>**YYYY-MM-DD_HH-SS_newMails_[IhreKlasse_IhrNachname].zip** (oder .tar oder .rar je nach Technik)



### Stufe 4

Später werden diese obigen Prozesse für kommende Anwenungen 
vollautomatisch erstellt werden. Dafür erstellen Sie

- 1.) ein CronTab-Eintrag in Linux<br>(oder Aufgabenplanungs-Task bein Windows)
- 2.) ein Mail mit Attachment 

damit Sie wissen, wann die Generierung der Liste und die Briefe
fertig ist und damit die Resultate an den richtigen Ort gelangen.

Das erzeugte Mail brauchen Sie auch, damit Sie die Resultate in einem 
ersten Ausbau mal an sich selber schicken und überprüfen können. 
Später könnten Sie dann das Mail an die **Zielperson** schicken, die 
es dann verarbeiten und verwalten muss.
<br>Gestalten Sie das Mail mit folgendem Text und hängen Sie die Archiv-Datei als
"attachment" an.

Die *Zielperson* kann zu Überprüfungszwecke auch Ihre Lehrperson sein.

*Beachten Sie: Sie haben normalerweise kein Mailserver auf Ihrem Rechner (oder VM).
Sie müssen entweder einen solchen (zusätzlich) installieren oder Sie benutzen eine
"Wegwerf-Mailadresse" über einen Gratis-Provider (z.B. gmx.ch, gmail.com, ...). Ihre
TBZ-Mailadresse können Sie nicht nehmen, da das von der Security her unterbunden wird.*
<br>
<br>[Mail mit PowerShell](../tools-technics/mailing-mit-powershell.jpg)
<br>[Mail mit Python](../tools-technics/mailing-mit-python.jpg)

Subject:

	Neue TBZ-Mailadressen [NumberOfNewMails]

Body:

	Lieber [wählen sie selber eine Anrede je nach Adressat]

	Die Emailadressen-Generierung ist beendet. 
	Es wurden [NumberOfNewMails] erzeugt.

	Bei Fragen kontaktiere bitte [IhreTBZ-Emailadresse]

	Gruss [IhrVorname] [IhrNachname]

Attachment:

	YYYY-MM-DD_HH-SS_newMails_[IhreKlasse_IhrNachname].zip


### Stufe 5

Die Archiv-Datei schicken Sie nun auf einen fremden Rechner mittels **FTP**. (Von der 
sicheren Datenübertragung wie FTPs sehen wir hier mal ab damit Sie nicht zusätzlich 
ausgebremst werden).
Benutzen Sie bitte folgende Zugangsdaten:


Browserzugang zum Testen oder Nachschauen:

[https://haraldmueller.ch/schueler](https://haraldmueller.ch/schueler) (Passwort: "tbz")


FTP-Zugangsdaten:

	HOST: "ftp.haraldmueller.ch"
	USER: "schueler"
	PASS: "studentenpasswort"
	PATH: "/M122-[IhreKlasse]" (z.B. /M122-AP22b)
		


### Stufe 6

Schicken Sie zwei bis drei der erstellten Briefe an Ihren Standard-Drucker. 
(Tipp: Stellen Sie Ihren Standard-Drucker auf "Print to PDF" ein, damit Sie 
besser testen können und zudem auch kein Papier verbrauchen müssen)


### Stufe 7

Finden Sie heraus, was man machen muss, wenn es in der Liste zwei Personen
hat, die den gleichen Vornamen und Nachnamen haben (solls ja geben).
Wie teilen Sie wem welche Emailadresse zu?


### Stufe 8

Sie binden Ihr Skript in den Scheduler (CronTab, Aufgabenplaner) ein. Die 
Input-Daten bekommen Sie vom gleichen Ort wie in Stufe 5. 
Sie müssen sie via FTP abholen.


### Abgabe-Bedingung

Der Abgabezeitraum gibt die Lehrperson bekannt und 
ist etwa **30 Minuten** lang. In dieser Zeit muss 
ihr System **über ein Start-Skript** laufen.
Eingriffe "von Hand" sind nicht erlaubt. <mark>Es zählt, was Ihr
System zum Abgabe- und Testzeitpunkt leistet</mark>. Für diesen
**Abschluss-Test** wird von Ihrer Lehrperson eine 
**separates Test-Datei** in der gleichen Form bereitgestellt, die dann 
korrekt verarbeitet werden soll.


## Bewertung

| Stufe | Beschreibung | Punkte |
|-------|--------------|--------|
|     1 | Datei mit Mailadr./Passw. liegt vor |  4 |
|     2 | Alle Briefe korrekt erstellt        |  4 |
|     3 | Archiv-Datei erstellt               |  1 |
|     4 | Korrektes Mail kommt samt Att. an   |  4 |
|     5 | FTP-Transfer kommt korrekt an       |  3 |
|     6 | Briefe liegen auf dem Drucker       |  1 |
|     7 | Dublettenkontrolle                  |  2 |
|     8 | Vollautomatische Verarbeitung       |  2 |
| Total |                                   | **20** |


### Noten

| Note| Punkte    |
|-----|-----------|
| 6.0 | über 19.0 |
| 5.5 | 17.0-19.0 |
| 5.0 | 15.0-16.5 | 
| 4.5 | 13.0-14.5 |
| 4.0 | 11.0-12.5 |
| 3.5 |  9.0-10.5 |
| 1.0 |Nichtabgabe|


# Howto organize Common Config and Common functions

The skripts `bin/common_functions.bash` and `bin/common_variables.bash` are the places to set your common variables and functions.

Additionally configuration Variables can be set in `etc/<yourscriptname>.conf` for a specific script

This directory structure is ment as a good start how to organize your script.
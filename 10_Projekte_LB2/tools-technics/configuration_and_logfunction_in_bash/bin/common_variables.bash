cwd=`pwd`               # current working directory
cd `dirname $0` # change to the directory where the script is located
BINDIR=`pwd`    # BINDIR: the directory where the script is located
cd $cwd         # return to the working directory
ETCDIR=$BINDIR/../etc # ETCDIR is the config directory
VARDIR=$BINDIR/../var # VARDIR is the directory with changing content
BASENAME=$(basename $0)
if [ -r $ETCDIR/$BASENAME.conf ] ; then
  . $ETCDIR/$BASENAME.conf        # run config file Konfigdatei
fi
if [ -z $TMPDIR ] ; then
  TMPDIR=/tmp/$BASENAME.$$
fi
LOGLEVEL=I
LOGFILE=$VARDIR/$BASENAME.log
